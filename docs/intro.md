# This is the IGE jupyter-book demo

# Link to tutorial

[One makdown page](markdown.md) : This is a makdown page
[One notbook page]

# Exercise

1. Clone me
2. Add a page using only markdown file `.md`. Don't forget to use [fancy box](https://jupyterbook.org/en/stable/content/content-blocks.html) ! Because it **looks good**.
3. Push me and look at the magic : Your page is included in the html website
4. Add a page that is a notebook `.ipynb`
5. Add link to the `_toc.yml`
6. Push me

```{note}
You know every thing you need to use jupyter book```
